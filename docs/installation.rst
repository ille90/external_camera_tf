=============
Installation
=============

ROS Dependancies
================

Install the OpenCV3 package for ROS

.. code-block:: bash

     sudo apt-get install ros-indigo-opencv3

Install the usb_camera/uvc_camera drivers

.. code-block:: bash

     sudo apt-get install ros-indigo-usb-cam
     sudo apt-get install ros-indigo-uvc-camera

Package Installation
====================

Clone and build the package from source into your catkin workspace

.. code-block:: bash

     cd ~/catkin_ws/src
     git clone https://ille90@bitbucket.org/ille90/external_camera_tf.git
     cd ~/catkin_ws/src/external_camera_tf
     catkin build --this
