==============================
External Camera System: Design
==============================


Mounting System
======================

The .stl files can be found at:
http://www.thingiverse.com/thing:1454407


.. image:: png/Bild4.png
   :alt: A sample output
   :align: center

.. image:: png/Bild3.png
   :alt: A sample output
   :align: center

Physical Installation
=====================



Transmitter-Receiver Components
-------------------------------

.. image:: png/tabelle.PNG
   :alt: table
   :align: center

Transmitter
-----------

.. image:: png/Bild5.png
   :alt: transmitter
   :align: center

Receiver
-----------

.. image:: png/Bild6.png
   :alt: receiver
   :align: center



Electrical Installation
=======================

.. image:: png/transmitter_receiver_sketch.png
   :alt: A sample output
   :align: center
