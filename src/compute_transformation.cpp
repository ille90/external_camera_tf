/*
 * viz_transformation.cpp
 *      This node uses the cv::viz module Viz3d to show the Transformation between two images from 2 different cameras
 *      It also computes the Pose between them and saves it in a file.
 *
 *  Created on: Feb 2, 2016
 *      Author: ille
 */

/*--- OpenCV includes ---*/
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/core/core.hpp"
#include <opencv2/opencv.hpp>
#include <opencv2/calib3d.hpp>

/*--- ROS includes ---*/
#include "ros/ros.h"
#include "std_msgs/String.h"
#include <tf/transform_broadcaster.h>

using namespace std;

// 1. Paths to the camera_info files
static const string ARDRONE_FRONT_CAMERA =
    "/home/ille/catkin_ws/src/external_camera_tf/data/camera_info/ardrone_front.yaml";
static const string ARDRONE_BOTTOM_CAMERA =
    "/home/ille/catkin_ws/src/external_camera_tf/data/camera_info/ardrone_bottom.yaml";
static const string ARDRONE_EXTERNAL_CAMERA =
    "/home/ille/catkin_ws/src/external_camera_tf/data/camera_info/ardrone_external.yaml";

// 2. Size and squareSize of the checkerboard
double squareSize = 0.0359375;
cv::Size chessboardSize = cv::Size(8, 6);

// 3. Switch-case for camera intrinsics: external+front=0; external+bottom=1
int caseSwitch = 0;

/* 4. Import Settings:
 *      IMAGE_SOURCE1 = the path to the images
 *      IMAGE_BASE_NAME1 = base name of the images, without the number
 *      IMAGE_TYPE1 = type of the image
 */
static const string IMAGE_SOURCE1 = "/home/ille/catkin_ws/src/external_camera_tf/data/images/front/";
static const string IMAGE_BASE_NAME1 = "front_cam";
static const string IMAGE_TYPE1 = ".jpg";

static const string IMAGE_SOURCE2 = "/home/ille/catkin_ws/src/external_camera_tf/data/images/external/";
static const string IMAGE_BASE_NAME2 = "external_cam";
static const string IMAGE_TYPE2 = ".jpg";

/* 5. Output:
 *      PARAMETERS1 = R|t , rotation and translation for camera1
 *      PARAMETERS2 = R|t , rotation and translation for camera2
 *      POSE = R|T, rotation and translation between camera1 and camera2.
 *
 */
static const string SINGLE_POSE1 = "/home/ille/catkin_ws/src/external_camera_tf/data/images/front/single_pose.yaml";
static const string WORLD_POINTS1 = "/home/ille/catkin_ws/src/external_camera_tf/data/images/front/points.yaml";

static const string SINGLE_POSE2 = "/home/ille/catkin_ws/src/external_camera_tf/data/images/external/single_pose.yaml";
static const string WORLD_POINTS2 = "/home/ille/catkin_ws/src/external_camera_tf/data/images/external/points.yaml";

static const string POSE = "/home/ille/catkin_ws/src/external_camera_tf/data/images/external/pose.yaml";


int fileIndex1 = 0;
int fileIndex2 = 0;
vector<cv::Mat> img_vec2, img_vec1; // vec for images
vector<vector<cv::Point2f> > cornersVEC1, cornersVEC2;   // vec for corners, image points
vector<cv::Point3f> objectPoints;   // 3D points in chessboard coordinate frame
vector<vector<cv::Point3f> > objVector;

// vector for Rotation vector and Translation vector
vector<cv::Mat> rvecVEC, tvecVEC;
vector<cv::Mat> rvecVEC2, tvecVEC2;
// Rotation matrix and transposed Rotation matrix
vector<cv::Mat> rmatVEC, rmatVEC2;
vector<cv::Mat> rmatVEC_tran, rmatVEC_tran2;
cv::Mat intrinsics, distortion, intrinsics2, distortion2;

/*-- import bottom cam images --*/
void importBottom()
{
  cv::Mat img_current;
  bool check = false;
  while (!check)
  {
    std::stringstream ss;
    // location of the images
    ss << IMAGE_SOURCE1 << IMAGE_BASE_NAME1 << fileIndex1 << IMAGE_TYPE1;
    img_current = imread(ss.str(), cv::IMREAD_GRAYSCALE); // read image

    if (img_current.data)
    {
      // Check for invalid input
      img_vec1.push_back(img_current);
      fileIndex1++;
    }
    else
      check = true;
  }
}

/*-- import front cam images --*/
void importFront()
{
  cv::Mat img_current;

  bool check = false;
  while (!check)
  {
    std::stringstream ss;
    // location of the images
    ss << IMAGE_SOURCE1 << IMAGE_BASE_NAME1 << fileIndex1 << IMAGE_TYPE1;
    // saves the images if data is okay
    img_current = imread(ss.str(), cv::IMREAD_GRAYSCALE);

    // Check for invalid input
    if (img_current.data)
    {
      // Check for invalid input
      img_vec1.push_back(img_current);
      fileIndex1++;
    }
    else
      check = true;
  }
}

/*-- import external cam images --*/
void importExternal()
{
  cv::Mat img_current;
  bool check = false;
  while (!check)
  {
    std::stringstream ss;
    // location of the images
    ss << IMAGE_SOURCE2 << IMAGE_BASE_NAME2 << fileIndex2 << IMAGE_TYPE2;
    // saves the images if data is okay
    img_current = imread(ss.str(), cv::IMREAD_GRAYSCALE);

    // Check for invalid input
    if (img_current.data)
    {
      // Check for invalid input
      img_vec2.push_back(img_current);
      fileIndex2++;
    }
    else
      check = true;
  }

}

// import intrinsics of two cameras
void importIntrinsics()
{
  try
  {
    if (caseSwitch == 0)
    {
      cv::FileStorage fs(ARDRONE_FRONT_CAMERA, cv::FileStorage::READ);
      fs["camera_matrix"] >> intrinsics;
      fs["distortion_coefficients"] >> distortion;

      cv::FileStorage fs2(ARDRONE_EXTERNAL_CAMERA, cv::FileStorage::READ);
      fs2["camera_matrix"] >> intrinsics2;
      fs2["distortion_coefficients"] >> distortion2;

    }
    else if (caseSwitch == 1)
    {
      cv::FileStorage fs(ARDRONE_BOTTOM_CAMERA, cv::FileStorage::READ);
      fs["camera_matrix"] >> intrinsics;
      fs["distortion_coefficients"] >> distortion;

      cv::FileStorage fs2(ARDRONE_EXTERNAL_CAMERA, cv::FileStorage::READ);
      fs2["camera_matrix"] >> intrinsics2;
      fs2["distortion_coefficients"] >> distortion2;
    }
  }
  catch (exception &e)
  {
    cout << " error reading camara_info.yaml" << e.what() << endl;
  }

}

// extract all corners from the image vectors
void getCorners()
{
  for (int index = 0; index < int(img_vec1.size()); index++)
  {
    // instances for current elements
    vector<cv::Point2f> corner_current, corner_current2;
    bool found_current, found_current2;

    // check for chessboard corners
    found_current = cv::findChessboardCorners(img_vec1.at(index), chessboardSize, corner_current,
                                              cv::CALIB_CB_ADAPTIVE_THRESH + cv::CALIB_CB_NORMALIZE_IMAGE);
    found_current2 = cv::findChessboardCorners(img_vec2.at(index), chessboardSize, corner_current2,
                                               cv::CALIB_CB_ADAPTIVE_THRESH + cv::CALIB_CB_NORMALIZE_IMAGE);

    // improve found patter corners
    cv::cornerSubPix(img_vec1.at(index), corner_current, cv::Size(7, 7), cv::Size(-1, -1),
                     cv::TermCriteria(cv::TermCriteria::EPS + cv::TermCriteria::COUNT, 100, 1e-5));
    cv::cornerSubPix(img_vec2.at(index), corner_current2, cv::Size(7, 7), cv::Size(-1, -1),
                     cv::TermCriteria(cv::TermCriteria::EPS + cv::TermCriteria::COUNT, 100, 1e-5));

    // convert to BGR to draw Colored chessboard corners
    cv::cvtColor(img_vec1.at(index), img_vec1.at(index), cv::COLOR_GRAY2BGR);
    cv::cvtColor(img_vec2.at(index), img_vec2.at(index), cv::COLOR_GRAY2BGR);

    // draw the detected corners1 in color
    drawChessboardCorners(img_vec1.at(index), chessboardSize, corner_current, found_current);
    drawChessboardCorners(img_vec2.at(index), chessboardSize, corner_current2, found_current2);

    // only fill vectors if corners were found on both images
    if (found_current && found_current2)
    {
      cornersVEC1.push_back(corner_current);
      cornersVEC2.push_back(corner_current2);
    }
    else
      cerr << "bad image found, delete it, index: " << index << endl << endl;
  }
}

// get Extrinsics from the images
void getExtrinsic()
{
  getCorners();
  int booard_h = chessboardSize.height;
  int booard_w = chessboardSize.width;

  // fill objectPoints vector with chessboard points
  for (int y = 0; y < booard_h; ++y)
  {
    for (int x = 0; x < booard_w; ++x)
      objectPoints.push_back(cv::Point3f(x * squareSize, y * squareSize, 0));
  }
  while (objVector.size() != cornersVEC1.size())
  {
    objVector.push_back(objectPoints);
  }

  // fill rvecVEC and tvecVEC
  for (int index = 0; index < int(img_vec1.size()); index++)
  {
    cv::Mat rvec_current, rvec_current2;
    cv::Mat tvec_current, tvec_current2;
    bool solve_current, solve_current2;

    solve_current = cv::solvePnP(objectPoints, cornersVEC1.at(index), intrinsics, distortion, rvec_current,
                                 tvec_current, false, CV_ITERATIVE);
    solve_current2 = cv::solvePnP(objectPoints, cornersVEC2.at(index), intrinsics2, distortion2, rvec_current2,
                                  tvec_current2, false, CV_ITERATIVE);

    /*cv::solvePnPRansac(objectPoints, cornersVEC1.at(index), intrinsics, distortion, rvec_current,
     tvec_current, false, 100,
     0.50, 100,
     cv::noArray(), cv::ITERATIVE );

     cv::solvePnPRansac(objectPoints, cornersVEC2.at(index), intrinsics2, distortion2, rvec_current2,
     tvec_current2, false, 100,
     0.50, 100,
     cv::noArray(), cv::ITERATIVE );

     */

    if (solve_current && solve_current2)
    {
      rvecVEC.push_back(rvec_current);
      rvecVEC2.push_back(rvec_current2);
      tvecVEC.push_back(tvec_current);
      tvecVEC2.push_back(tvec_current2);
    }
    else
      cout << "could not find rvec and tvec, index: " << index << endl;
  }

  // convert Rotation vector to Rotation matrix
  for (int index = 0; index < int(img_vec1.size()); index++)
  {
    cv::Mat rvec_current = rvecVEC.at(index);
    cv::Mat rvec_current2 = rvecVEC2.at(index);
    cv::Mat rmat_current, rmat_current_tran, rmat_current2, rmat_current_tran2;

    cv::Rodrigues(rvec_current, rmat_current);
    cv::Rodrigues(rvec_current2, rmat_current2);
    cv::transpose(rmat_current, rmat_current_tran);
    cv::transpose(rmat_current2, rmat_current_tran2);

    rmatVEC.push_back(rmat_current);
    rmatVEC2.push_back(rmat_current2);
    rmatVEC_tran.push_back(rmat_current_tran);
    rmatVEC_tran2.push_back(rmat_current_tran2);
  }
  cout << "Vector for Rotation and Translation successfully converted " << endl;

}

// transform corners from world coordinate frame(chessboard coordinate frame) to camera coordinate frame
void transfromWorldToCamera()
{
  cv::FileStorage fsPose1(SINGLE_POSE1, cv::FileStorage::WRITE);
  cv::FileStorage fsPose2(SINGLE_POSE2, cv::FileStorage::WRITE);

  cv::FileStorage fsPoints1(WORLD_POINTS1, cv::FileStorage::WRITE);
  cv::FileStorage fsPoints2(WORLD_POINTS2, cv::FileStorage::WRITE);

  fsPose1 << "imgNumber" << int(rmatVEC.size());
  fsPose2 << "imgNumber" << int(rmatVEC2.size());

  for (int j = 0; j < int(rmatVEC.size()); j++)
  {
    cv::Mat rmat_current, rmat_current2;
    cv::Mat tvec_current, tvec_current2;

    cv::Point3f pointInChessFrame;
    cv::Mat cam_point, cam_point2;

    char file_name[100]; // each image gets a name group
    sprintf(file_name, "image%d", j);

    rmat_current = rmatVEC.at(j);
    tvec_current = tvecVEC.at(j);
    rmat_current2 = rmatVEC2.at(j);
    tvec_current2 = tvecVEC2.at(j);
    fsPose1 << "R" << rmat_current;
    fsPose1 << "t" << tvec_current;
    fsPose2 << "R" << rmat_current2;
    fsPose2 << "t" << tvec_current2;

    fsPoints1 << file_name << "[";
    fsPoints2 << file_name << "[";

    for (int i = 0; i < int(objectPoints.size()); i++)
    {
      pointInChessFrame = objectPoints.at(i); // a point in world (chessboard) coordinate frame
      cv::Mat world_point = (cv::Mat_<double>(3, 1) << pointInChessFrame.x, pointInChessFrame.y, pointInChessFrame.z); // convert a Point3f to Mat

      cam_point = rmat_current * world_point + tvec_current; // transform:point (X,Y,Z) from world coordinate frame to (x,y,z) camera coordinate frame
      cam_point2 = rmat_current2 * world_point + tvec_current2;

      /*--- uncomment to view all 3 points
       cout << "################################################" << endl << endl;
       cout << "world_point" << i << ": " << endl << world_point << endl << endl;

       cout << "cam_point" << i << ": " << endl << cam_point << endl << endl;
       cout << "cam2_point" << i << ": " << endl << cam_point2 << endl << endl;
       */
      fsPoints1 << cam_point;
      fsPoints2 << cam_point2;

    }
    // save to .yaml;
    fsPoints1 << "]";
    fsPoints2 << "]";

  }

  fsPose1.release();
  fsPose2.release();
  fsPoints1.release();
  fsPoints2.release();

}

void prepareData()
{
  // import the image files
  if (caseSwitch == 0)
  {
    importExternal();
    importFront();
  }
  else if (caseSwitch == 1)
  {
    importExternal();
    importBottom();
  }

  /*----- check imported data ------*/
  cout << "vec1.size: " << int(img_vec1.size()) << endl;
  cout << "vec2.size: " << int(img_vec2.size()) << endl;

  if (int(img_vec1.size()) != int(img_vec2.size()))
  {
    cerr << "cameras have different amount of images, check the folders" << endl;

  }

  importIntrinsics();
  getExtrinsic();
  transfromWorldToCamera();

  // show all images with detected corners
  int index = 0;
  while (index < (int)img_vec1.size())
  {
    cv::imshow("front/bottom", img_vec1.at(index));
    cv::imshow("external", img_vec2.at(index));
    cv::waitKey(0);
    index++;
  }
}

/*  uses the transformation equations to compute the Transform between the cameras
 *
 *  different to computeTransformation2() where all images are taken to compute the relative pose between the cameras,
 *  computeTransformation() computes the relative pose between two images
 *
 */
void computeTransform()
{
  prepareData();

  cv::FileStorage fsresult(POSE, cv::FileStorage::WRITE);
  vector<cv::Mat> rmat_solution, tvec_solution;
  cv::Mat R, T;
  for (int index = 0; index < int(rmatVEC.size()); index++)
  {
    cv::Mat rmat_tran_current2, rmat_current;
    cv::Mat tvec_current, tvec_current2;

    // initialize current elements
    rmat_tran_current2 = rmatVEC_tran2.at(index);
    rmat_current = rmatVEC.at(index);
    tvec_current = tvecVEC.at(index);
    tvec_current2 = tvecVEC2.at(index);

    cv::Point3f p(tvec_current); // convert cv::Mat to cv::Point3f
    cv::Point3f p2(tvec_current2);
    double a = p.z;
    double b = p2.z;
    cout << "distance tvec: " << a << endl;
    cout << "distance tvec2: " << b << endl;

    // equation for pose between 2 cameras or how to get from cam1 to cam2 in 3D space
    R = rmat_current * rmat_tran_current2;
    T = -1 * (rmat_current * rmat_tran_current2 * tvec_current2) + (tvec_current);
    rmat_solution.push_back(R);
    tvec_solution.push_back(T);

    /*--- convert OpenCV Rotation to ROS rotaion

     tf::Matrix3x3 tfR = tf::Matrix3x3(   R.at<double>(0, 0), R.at<double>(0, 0), R.at<double>(0, 0),
     R.at<double>(0, 0), R.at<double>(0, 0), R.at<double>(0, 0),
     R.at<double>(0, 0), R.at<double>(0, 0), R.at<double>(0, 0));

     ---*/

    cout << "R = " << endl << " " << cv::Mat(R) << endl << endl;
    cout << "T = " << endl << " " << cv::Mat(T) << endl << endl;

    /*--------- Saving Data -----*/
    // fsresult << "tfR" << tfR;
    fsresult << "R" << R;
    fsresult << "T" << T;
  }
  fsresult.release();

  transfromWorldToCamera();
  // distance between the poses
  vector<double> pose_distance;
  for (int index = 0; index < int(tvec_solution.size()); index++)
  {
    pose_distance.push_back(cv::norm(tvec_solution.at(index), cv::NORM_L2, cv::noArray())); // norm the elements, to get the distance
  }
  for (int index = 0; index < int(pose_distance.size()); index++) // cout the distance  between the two cameras and show the images
  {
    cout << "||tvec_solution" << index << "|| = " << endl << " " << pose_distance.at(index) << endl << endl;
    cv::imshow("vec1", img_vec1.at(index));
    cv::imshow("vec2", img_vec2.at(index));
    cv::waitKey(0);
  }
  cout << "#########END#########" << endl;
}

// uses the stereoCalibrate() function to compute the Transform between the cameras
void computeTransform2()
{
  prepareData();
  // R is the rotation matrix, T is the translation vector
  cv::Mat R, T, E, F;

  // all vectors must have the same size, else ERROR
  cout << "objvec: " << objVector.size() << endl;
  cout << "cornervec: " << cornersVEC1.size() << endl;
  cout << "cornervec2: " << cornersVEC2.size() << endl;

  double rms = cv::stereoCalibrate(objVector, cornersVEC1, cornersVEC2, intrinsics, distortion, intrinsics2,
                                   distortion2, chessboardSize, R, T, E, F, cv::CALIB_FIX_INTRINSIC,
                                   cv::TermCriteria(cv::TermCriteria::COUNT + cv::TermCriteria::EPS, 50, 1e-6));
  cout << "rms: " << rms << endl;
  cout << "||T_solution" << "|| = " << endl << " " << cv::norm(T, cv::NORM_L2, cv::noArray()) << endl;

  /*--------- Saving Data -----*/
  cv::FileStorage fsresult("/home/ille/catkin_ws/src/external_camera_tf/data/pose/pose.yaml", cv::FileStorage::WRITE);
  fsresult << "rms" << rms;
  fsresult << "R" << R;
  fsresult << "T" << T;
  cout << "#########END#########" << endl;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "talker");
  ros::NodeHandle n;
  ros::Publisher chatter_pub = n.advertise < std_msgs::String > ("chatter", 1);
  computeTransform();
  return 0;
}
;
