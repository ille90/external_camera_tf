/*
 *  This node subscribes to image_raw topics from two cameras and detects a chessboard.
 *
 *  Edit the folder path from bottom to front in the callback function like this:
 *  /home/ille/catkin_ws/src/external_camera_tf/data/images/bottom/bottom_cam%d.jpg
 *  /home/ille/catkin_ws/src/external_camera_tf/data/images/front/front_cam%d.jpg
 *
 *
 * ####Tutorial####
 * Step1: run this node when both cameras publish their topic with: rosrun external_camera_tf calib_gui
 * Step2: Ctrl+P or right Click on image and Click on "Display properties window" to show the save button
 * Step3: when both images show a detected chessboard AND the colors are displayed equally, click on the "save" button
 */

/*--- ROS includes ---*/
#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

/*--- OpenCV includes ---*/
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/core/core.hpp"
#include <opencv2/opencv.hpp>
#include <opencv2/calib3d.hpp>

using namespace std;

// 1. Topics this node subscribes to
static const string CAMERA1_TOPIC = "/cam_video/image_raw";
static const string CAMERA2_TOPIC = "/camera_external/image_raw";

// 2. Size of the checkerboard
cv::Size checkerboardSize = cv::Size(8, 6);


// Names for the two camera windows
static const string OPENCV_WINDOW1 = "Front/Bottom Camera Image Window";
static const string OPENCV_WINDOW2 = "External Camera Image Window";

// Topics that are published
static const string CAMERA1_TOPIC_OUTPUT = "/image_converter/output_ardrone";
static const string CAMERA2_TOPIC_OUTPUT = "/image_converter/output_external";

// variables for saving images
int fileIndex1 = 0;
int fileIndex2 = 0;
cv::Mat src1, src2;

/*----------------Callback function section for push button -----------------*/
void buttonCallback(int state, void* userdata)
{
  // save current image to folder
  char file_name1[100];
  char file_name2[100];
  sprintf(file_name1, "/home/ille/catkin_ws/src/external_camera_tf/data/images/bottom/bottom_cam%d.jpg", fileIndex1++);
  sprintf(file_name2, "/home/ille/catkin_ws/src/external_camera_tf/data/images/external/external_cam%d.jpg",
          fileIndex2++);

  // saves the images if data is okay
  if (src2.data && src1.data  )
  {
    imwrite(file_name1, src1);
    imwrite(file_name2, src2);
  }
}

class ImageConverter
{
  ros::NodeHandle nh_;
  image_transport::ImageTransport it_;
  image_transport::Subscriber image_sub_, image_sub2_;
  image_transport::Publisher image_pub_, image_pub2_;

public:
  // constructor
  ImageConverter() :
      it_(nh_)
  {
    // subscribe to input video feed and publish output video feed
    image_sub_ = it_.subscribe(CAMERA1_TOPIC, 1, &ImageConverter::imageCb, this);
    image_sub2_ = it_.subscribe(CAMERA2_TOPIC, 1, &ImageConverter::imageCb2, this);
    image_pub_ = it_.advertise(CAMERA1_TOPIC_OUTPUT, 1);
    image_pub2_ = it_.advertise(CAMERA2_TOPIC_OUTPUT, 1);

    cv::namedWindow(OPENCV_WINDOW1);
    cv::namedWindow(OPENCV_WINDOW2);
    cv::createButton("save", buttonCallback, &src1, cv::QT_PUSH_BUTTON, 1);
  }

  ~ImageConverter()
  {
    cv::destroyAllWindows();
  }

  /*-------- Callback functions for front/bottom camera  -----------*/
  void imageCb(const sensor_msgs::ImageConstPtr& msg)
  {
    cv_bridge::CvImagePtr cv_ptr;
    try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);

    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }

    // check for chessboard corners
    vector<cv::Point2f> corner_current;
    bool found_current;

    // convert image to gray-> function findChesboardCorners needs gray image
    cv::cvtColor(cv_ptr->image, cv_ptr->image, cv::COLOR_BGR2GRAY);
    found_current = findChessboardCorners(cv_ptr->image, checkerboardSize, corner_current,
                                          cv::CALIB_CB_ADAPTIVE_THRESH + cv::CALIB_CB_NORMALIZE_IMAGE);

    if (found_current)
    {
      // set current image as src1
      src1 = cv_ptr->image;
      cv::cornerSubPix(cv_ptr->image, corner_current, checkerboardSize, cv::Size(-1, -1),
                       cv::TermCriteria(cv::TermCriteria::EPS + cv::TermCriteria::COUNT, 50, 0.1));
      // convert image to color
      cv::cvtColor(cv_ptr->image, cv_ptr->image, cv::COLOR_GRAY2RGB);
      cv::drawChessboardCorners(cv_ptr->image, checkerboardSize, corner_current, found_current);

    }
    // Update GUI Window
    cv::imshow(OPENCV_WINDOW1, cv_ptr->image);

    cv::waitKey(3);

    // Output modified video stream
    image_pub_.publish(cv_ptr->toImageMsg());

  }

  /*-------- Callback functions for external camera  -----------*/
  void imageCb2(const sensor_msgs::ImageConstPtr& msg)
  {
    cv_bridge::CvImagePtr cv_ptr;
    try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);

    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }

    // check for chessboard corners
    vector<cv::Point2f> corner_current;
    bool found_current;

    // convert image to gray-> function findChesboardCorners needs gray image
    cv::cvtColor(cv_ptr->image, cv_ptr->image, cv::COLOR_BGR2GRAY);
    found_current = findChessboardCorners(cv_ptr->image, checkerboardSize, corner_current,
                                          cv::CALIB_CB_ADAPTIVE_THRESH + cv::CALIB_CB_NORMALIZE_IMAGE);

    if (found_current)
    {
      // set current image as src1
      src2 = cv_ptr->image;
      cv::cornerSubPix(cv_ptr->image, corner_current, checkerboardSize, cv::Size(-1, -1),
                       cv::TermCriteria(cv::TermCriteria::EPS + cv::TermCriteria::COUNT, 50, 0.1));
      // convert image to color
      cv::cvtColor(cv_ptr->image, cv_ptr->image, cv::COLOR_GRAY2RGB);
      cv::drawChessboardCorners(cv_ptr->image, checkerboardSize, corner_current, found_current);

    }
    // Update GUI Window
    cv::imshow(OPENCV_WINDOW2, cv_ptr->image);
    cv::waitKey(3);

    // Output modified video stream
    image_pub2_.publish(cv_ptr->toImageMsg());

  }
};

/*---- main function ----*/
int main(int argc, char** argv)
{
  ros::init(argc, argv, "image_converter");
  ImageConverter ic;
  ros::spin();
  return 0;
}
;
