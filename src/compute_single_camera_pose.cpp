/*
 * This node determines the rotation (as a 3x3 matrix and a 3x1 Rodrigues vector) and translation vector to a chessboard
 * from a the camera point of view and saves the result in a .yaml file
 *
 */

/*--OpenCV includes---*/
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/core/core.hpp"
#include <opencv2/opencv.hpp>
#include <opencv2/viz.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/core/affine.hpp>

/*--- Ros includes ---*/
#include "ros/ros.h"
#include "std_msgs/String.h"
#include <tf/transform_broadcaster.h>

using namespace std;

double squareSize = 0.0359375;
cv::Size chessboardSize = cv::Size(8, 6);

// 1. Paths to the camera_info
static const string CAMERA_INFO =
    "/home/ille/catkin_ws/src/external_camera_tf/data/camera_info/ardrone_external.yaml";

// 2. Paths where ONE image is imported from
static const string IMAGE_SOURCE =
    "/home/ille/catkin_ws/src/external_camera_tf/data/images/external/";
static const string IMAGE_BASE_NAME = "external_cam0";
static const string IMAGE_TYPE = ".jpg";

// 3. Save directory of the desired parameters (R|t) (Rotation|Translation)
static const string IMAGE_SINK =
    "/home/ille/catkin_ws/src/external_camera_tf/data/images/external/single_image.yaml";

int fileIndex = 0;
vector<cv::Mat> img_vec;
vector<vector<cv::Point2f> > cornersVEC;
vector<cv::Point3f> objectPoints;
vector<cv::Mat> rvecVEC, tvecVEC;
vector<cv::Mat> rmatVEC;
int booard_h = chessboardSize.height;
int booard_w = chessboardSize.width;
cv::Mat intrinsics, distortion;

/*-- import external cam images from a folder--*/
void importImages()
{
  bool check = false; // all images are found
  cv::Mat img_current;

  while (!check)
  {
    std::stringstream ss;
    // location of the images
    ss << IMAGE_SOURCE << IMAGE_BASE_NAME << fileIndex << IMAGE_TYPE;
    img_current = imread(ss.str(), cv::IMREAD_GRAYSCALE);

    if (img_current.data)
    {   // save the image if data is okay
      img_vec.push_back(img_current);
      imshow(ss.str(), img_current);
      cv::moveWindow(ss.str(), 100, 100);
      cout << ss.str() << endl;
      cv::waitKey(0);
      cv::destroyWindow(ss.str());
      fileIndex++;

    }
    else
      check = true;
  }
}

// import intrinsics
void importIntrinsics()
{
  try
  {
    cv::FileStorage fs(CAMERA_INFO, cv::FileStorage::READ);
    fs["camera_matrix"] >> intrinsics;
    fs["distortion_coefficients"] >> distortion;
  }
  catch (exception &e)
  {
    cout << " error reading camara_info.yaml" << e.what() << endl;
  }

}

// get Extrinsic from the images
void getExtrinsic()
{
  // fill objectPoints vector with chessboard points
  for (int y = 0; y < booard_h; ++y)
  {
    for (int x = 0; x < booard_w; ++x)
      objectPoints.push_back(cv::Point3f(x * squareSize, y * squareSize, 0));
  }

  cv::FileStorage fs(IMAGE_SINK, cv::FileStorage::WRITE);

  for (int index = 0; index < int(img_vec.size()); index++)
  {
    std::stringstream ss, ss2;
    ss << IMAGE_SOURCE << IMAGE_BASE_NAME << index << IMAGE_TYPE;
    ss2 << IMAGE_BASE_NAME << index;

    // instances for current elements
    vector<cv::Point2f> corner_current;
    cv::Mat rvec_current, tvec_current, rmat_current;
    bool found_current, solve_current;

    // check for chessboard corners
    found_current = cv::findChessboardCorners(img_vec.at(index), chessboardSize, corner_current,
                                              cv::CALIB_CB_ADAPTIVE_THRESH + cv::CALIB_CB_NORMALIZE_IMAGE);
    // improve found patter corners
    cv::cornerSubPix(img_vec.at(index), corner_current, cv::Size(7, 7), cv::Size(-1, -1),
                     cv::TermCriteria(cv::TermCriteria::EPS + cv::TermCriteria::COUNT, 100, 1e-5));
    // convert to BGR to draw Colored chessboard corners
    cv::cvtColor(img_vec.at(index), img_vec.at(index), cv::COLOR_GRAY2BGR);

    // draw detected corners in color
    drawChessboardCorners(img_vec.at(index), chessboardSize, corner_current, found_current);

    cv::Mat undist;
    undistort(img_vec.at(index), undist, intrinsics, distortion, cv::noArray());
    imshow(ss.str(), img_vec.at(index));
    cv::waitKey(0);
    imshow(ss.str(), undist);
    cv::moveWindow(ss.str(), 100, 100);
    cv::waitKey(0);
    cout << ss.str() << " ";
    cv::destroyWindow(ss.str());

    // only fill vectors if corners were found on both images
    if (found_current)
    {
      solve_current = cv::solvePnP(objectPoints, corner_current, intrinsics, distortion, rvec_current, tvec_current,
                                   false, CV_ITERATIVE);
      /*cv::solvePnPRansac(objectPoints, cornersVEC.at(index), intrinsics, distortion, rvec_current,
       tvec_current, false, 100,
       0.50, 100,
       cv::noArray(), cv::ITERATIVE );
       */

      cv::Point3f p(tvec_current); // create Point3f point from Mat point to easier access to data
      double a = p.z;
      cout << "distance: " << a << endl;

      if (solve_current)
      {
        cv::Rodrigues(rvec_current, rmat_current);
        // fs << ss2.str() << "[";
        //  fs << "rmat_current" << rmat_current;
        fs << "rvec_current" << rvec_current;
        fs << "tvec_current" << tvec_current;
        //fs << "distance"<< a;
        //fs << "]";
      }

    }
    else
      cerr << "bad image found, delete it, index: " << index << endl << endl;
  }
  fs.release();
}

void oneImage()
{
  cv::Mat img_current;
  std::stringstream ss;
  // location of the images
  ss << IMAGE_SOURCE << IMAGE_BASE_NAME << IMAGE_TYPE;
  img_current = imread(ss.str(), cv::IMREAD_GRAYSCALE);

  if (img_current.data)
  {   // save the image if data is okay
    img_vec.push_back(img_current);
  }

  importIntrinsics();
  // fill objectPoints vector with chessboard points
  for (int y = 0; y < booard_h; ++y)
  {
    for (int x = 0; x < booard_w; ++x)
      objectPoints.push_back(cv::Point3f(x * squareSize, y * squareSize, 0));
  }

  cv::FileStorage fs(IMAGE_SINK, cv::FileStorage::WRITE);

  for (int index = 0; index < int(img_vec.size()); index++)
  {
    std::stringstream ss, ss2;
    ss << IMAGE_SOURCE << IMAGE_BASE_NAME << IMAGE_TYPE;
    ss2 << IMAGE_BASE_NAME;

    // instances for current elements
    vector<cv::Point2f> corner_current;
    cv::Mat rvec_current, tvec_current, rmat_current;
    bool found_current, solve_current;

    // check for chessboard corners
    found_current = cv::findChessboardCorners(img_vec.at(index), chessboardSize, corner_current,
                                              cv::CALIB_CB_ADAPTIVE_THRESH + cv::CALIB_CB_NORMALIZE_IMAGE);

    // improve found patter corners
    cv::cornerSubPix(img_vec.at(index), corner_current, cv::Size(7, 7), cv::Size(-1, -1),
                     cv::TermCriteria(cv::TermCriteria::EPS + cv::TermCriteria::COUNT, 100, 1e-3));

    // convert to BGR to draw Colored chessboard corners
    cv::cvtColor(img_vec.at(index), img_vec.at(index), cv::COLOR_GRAY2BGR);

    // draw detected corners in color
    drawChessboardCorners(img_vec.at(index), chessboardSize, corner_current, found_current);

    cv::Mat undist;
    undistort(img_vec.at(index), undist, intrinsics, distortion, cv::noArray());
    imshow(ss.str(), img_vec.at(index));

    imshow(ss.str(), undist);
    cv::moveWindow(ss.str(), 100, 100);
    cv::waitKey(0);
    cout << ss.str() << " "<<endl<<endl;
    cv::destroyWindow(ss.str());

    // only fill vectors if corners were found on both images
    if (found_current)
    {
      solve_current = cv::solvePnP(objectPoints, corner_current, intrinsics, distortion, rvec_current, tvec_current,
                                   false, CV_ITERATIVE);
      /*cv::solvePnPRansac(objectPoints, cornersVEC.at(index), intrinsics, distortion, rvec_current,
       tvec_current, false, 100,
       0.50, 100,
       cv::noArray(), cv::ITERATIVE );
       */
/*
      cv::Point3f p(tvec_current); // convert cv::Mat to cv::Point3f
      double a = p.z;
      cout << "z component of the translation vector: " << a << endl;
*/
      if (solve_current)
      {
        cv::Rodrigues(rvec_current, rmat_current);

        fs << "rmat_current" << rmat_current;
        fs << "rvec_current" << rvec_current;
        fs << "tvec_current" << tvec_current;

      }
    }
    else
      cerr << "bad image found, delete it, index: " << index << endl << endl;
  }
  fs.release();
}

// node is executed only one time
int main(int argc, char **argv)
{
  ros::init(argc, argv, "talker");
  ros::NodeHandle n;
  ros::Publisher chatter_pub = n.advertise<std_msgs::String>("chatter", 1);
  oneImage();
  return 0;
}
;
