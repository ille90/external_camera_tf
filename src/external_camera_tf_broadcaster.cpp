/*
 *  simple tf broadcaster for the external camera. The parent tf frame is the front camera of the AR.DRone.
 *
 */

/*--- ROS includes ---*/
#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
using namespace std;

// 1. Parent coordinate frame
static const string PARENT_CAM ="ardrone_base_frontcam";

int main(int argc, char** argv)
{
  ros::init(argc, argv, "external_tf_broadcaster");
  ros::NodeHandle node;

  tf::TransformBroadcaster tf_broad;
  tf::Transform transform;
  tf::StampedTransform tf_bottom_external;

  ros::Rate rate(10.0);
  while (node.ok())
  {

    // copy&paste R and T from pose.yaml
    tf::Matrix3x3 R = tf::Matrix3x3( 9.9789799337188068e-01, 2.6072910076787169e-03,
                                     -6.4751809688806963e-02, -6.1357256215213542e-03,
                                     9.9850294377488946e-01, -5.4352774942752063e-02,
                                     6.4513159087674535e-02, 5.4635824387313209e-02,
                                     9.9642008159111639e-01 );

    tf::Vector3 T = tf::Vector3(1.7364865838495314e-01, -2.3857123863684243e-02,
                                3.4440008732199434e-03);

    // initialize transformation of Rotation and Translation
    transform = tf::Transform(R, T);

    // initialize StampedTransform with parent ardrone_base_bottomcam to the new child ardrone_external_cam
    tf_bottom_external = tf::StampedTransform(transform, ros::Time::now(), PARENT_CAM,
                                              "ardrone_base_externalcam");

    // broadcast transfrom
    tf_broad.sendTransform(tf_bottom_external);
    rate.sleep();
  }
  return 0;
}
;
