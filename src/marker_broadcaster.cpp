/*
 *  This node broadcasts marker points from detected checkerboard corners of two images of two cameras.
 *
 *
 * ####Tutorial####
 * Step1: rosrun external_camera_tf_broadcaster // edit R|T for imageX
 * Step2: rosrun external_camera_tf marker_broadcaster
 *
 */

/*--- ROS includes ---*/
#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <tf/transform_broadcaster.h>

/*--- OpenCV includes ---*/
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/core/core.hpp"
#include <opencv2/core/affine.hpp>
#include "opencv2/opencv.hpp"

using namespace std;

// 1. Chessboard points of the image pair to broadcast
static const string IMAGE = "image3";

// 2. Camera IDs, from ardrone_driver and external_camera_tf_broadcaster
static const string CAMERA1 = "/ardrone_base_frontcam";
static const string CAMERA2 = "/ardrone_base_externalcam";

// 3. Path to the file with the objectpoints
static const string OBJ_POINTS1 = "/home/ille/catkin_ws/src/external_camera_tf/data/images/front/points.yaml";
static const string OBJ_POINTS2 = "/home/ille/catkin_ws/src/external_camera_tf/data/images/external/points.yaml";

visualization_msgs::Marker points1, points2;

void vizFrontMarker()
{
  visualization_msgs::Marker points;
  points.header.frame_id = CAMERA1;
  points.header.stamp = ros::Time::now();
  points.ns = "tud_coop_uv_coverage";
  points.action = visualization_msgs::Marker::ADD;
  points.type = visualization_msgs::Marker::POINTS;
  points.pose.orientation.w = 1.0;
  points.id = 0;
  points.scale.x = 0.01;
  points.scale.y = 0.01;
  points.color.r = 1.0;
  points.color.g = 0.0;
  points.color.b = 1.0;
  points.color.a = 1.0;



  cv::FileStorage fs(OBJ_POINTS1, cv::FileStorage::READ); // import points
  vector<cv::Mat> objPointsInCamFrame;
  fs[IMAGE] >> objPointsInCamFrame;

  geometry_msgs::Point point_cur;
  cv::Mat cam_point;

  for (int i = 0; i < int(objPointsInCamFrame.size()); i++)
  {
    cam_point = objPointsInCamFrame.at(i);

    cv::Point3f p(cam_point); // convert from cv::Mat to cv::Point3f

    point_cur.x = p.x;  // convert from cv::Point3f to geometry_msgs::Point
    point_cur.y = p.y;
    point_cur.z = p.z;

    points.points.push_back(point_cur);
  }

  points1 = points; // fill global points

}

void vizExternalMarker()
{
  visualization_msgs::Marker points;
  points.header.frame_id = CAMERA2;
  points.header.stamp = ros::Time::now();
  points.ns = "tud_coop_uv_coverage";
  points.action = visualization_msgs::Marker::ADD;
  points.type = visualization_msgs::Marker::POINTS;
  points.pose.orientation.w = 1.0;
  points.id = 1;
  points.scale.x = 0.01;
  points.scale.y = 0.01;
  points.color.r = 1.0;
  points.color.g = 1.0;
  points.color.b = 0.0;
  points.color.a = 1.0;

  cv::FileStorage fs(OBJ_POINTS2, cv::FileStorage::READ); // import points
  vector<cv::Mat> objPointsInCamFrame;
  fs[IMAGE] >> objPointsInCamFrame;

  cv::Mat cam_point;
  geometry_msgs::Point point_cur;

  for (int i = 0; i < int(objPointsInCamFrame.size()); i++)
  {
    cam_point = objPointsInCamFrame.at(i);

    cv::Point3f p(cam_point); // convert cv::Mat to cv::Point3f

    // convert Point3f to geometry_msgs
    point_cur.x = p.x;
    point_cur.y = p.y;
    point_cur.z = p.z;

    points.points.push_back(point_cur);
  }

  points2 = points;

}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "basic_shapes");
  ros::NodeHandle n;
  ros::Rate r(1);
  ros::Publisher marker_pub = n.advertise<visualization_msgs::Marker>("visualization_marker", 1);

  while (ros::ok())
  {
    vizFrontMarker();
    vizExternalMarker();

    while (marker_pub.getNumSubscribers() < 1)
    {
      if (!ros::ok())
      {
        return 0;
      }
      ROS_WARN_ONCE("Please create a subscriber to the marker");
      sleep(1);
    }

    marker_pub.publish(points1);
    marker_pub.publish(points2);
    r.sleep();
  }
}
