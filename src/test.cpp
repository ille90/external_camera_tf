/*
 *   tf broadcaster for the pose between the cameras
 *
 */

/*--- ROS includes ---*/
#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PointStamped.h>

using namespace std;

// topic name of the pose
static const string CAME_POSE ="vrep/pose";



#include "std_msgs/String.h"

// chatterCallback for Listener

void chatterCallback(const geometry_msgs::PoseStamped& msg)
{
        geometry_msgs::Point coord = msg.pose.position;
        ROS_INFO("Current position: (%g, %g, %g)", coord.x, coord.y, coord.z);
}

int main(int argc, char **argv)
{

  ros::init(argc, argv, "pose_broadcaster");

  // listener
  tf::TransformListener listener;
  ros::NodeHandle node;
  ros::Subscriber sub = node.subscribe("/vrep/posetopic", 1, chatterCallback);


  // publisher p
  tf::TransformBroadcaster tf_broad;
  tf::Transform transform;
  tf::StampedTransform tf_bottom_external;

  ros::Rate rate(10.0);

  while (node.ok())
  {



    // get info from subscribed topic
    tf::Quaternion q=tf::Quaternion(1,1,1,11);
    tf::Matrix3x3 R = tf::Matrix3x3(q);
    tf::Vector3 T = tf::Vector3(1.7364865838495314e-01, -2.3857123863684243e-02,
                                3.4440008732199434e-03);
    transform = tf::Transform(R, T);

    // initialize StampedTransform with parent ardrone_base_bottomcam to the new child ardrone_external_cam
    tf_bottom_external = tf::StampedTransform(transform, ros::Time::now(), "base",
                                              "base_child");
    // broadcast transfrom
    tf_broad.sendTransform(tf_bottom_external);
    rate.sleep();
  }
  return 0;
}
;
