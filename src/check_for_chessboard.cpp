/*
 *
 *  This node subscribes to ONE image_topic and checks with OpenCV for a chessboard/circleboard.
 *  The detected corners are published as Marker points and can be visualized with RVIZ in 3D.
 *
 *  To run this node, 6 Settings have to be edited, listed below.
 *
 *  Then run this node with: rosrun external_camera_tf check_for_chessboard
 *
 *
 *  Created on: Feb 2, 2016
 *      Author: ille.tu.da@googlemail.com
 */

/*--- ROS includes ---*/
#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <visualization_msgs/Marker.h>
#include <tf/transform_broadcaster.h>
#include <sensor_msgs/image_encodings.h>

/*--- OpenCV includes ---*/
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/core/core.hpp"
#include <opencv2/opencv.hpp>
#include <opencv2/calib3d.hpp>

using namespace std;
static const string OPENCV_WINDOW = "Image window";

// 1. Paths to the camera_info files, containing the intrinsic parameter of the cameras
static const string ARDRONE_FRONT_CAMERA =
    "/home/ille/catkin_ws/src/external_camera_tf/data/camera_info/ardrone_front.yaml";
static const string ARDRONE_BOTTOM_CAMERA =
    "/home/ille/catkin_ws/src/external_camera_tf/data/camera_info/ardrone_bottom.yaml";
static const string ARDRONE_EXTERNAL_CAMERA =
    "/home/ille/catkin_ws/src/external_camera_tf/data/camera_info/ardrone_external.yaml";

// 2. Switch-case for camera intrinsics: 0=external,1=front,2=bottom
int caseSwitch = 0;

// 3. Detect chessboard=0 or circleboard=1
int caseSwitchBoard = 0;

// 4. Image topic to subscribe to
static const string CAMERA1_TOPIC = "/ardrone/image_raw";
static const string CAMERA2_TOPIC = "/camera_external/image_raw";

// 5. Chessboard characteristics; squareSize in meter
double squareSize = 0.0359375;
cv::Size chessboardSize = cv::Size(8, 6);




// Image topic that will be published
static const string OUTPUT_IMAGE = "/image_converter/output_video";

cv::Mat intrinsics, distortion;
vector<cv::Point3f> objectPoints;
static string MARKER_BROADCASTER;

class ImageConverter
{
  ros::NodeHandle nh_;
  ros::Publisher marker_pub;
  image_transport::ImageTransport it_;
  image_transport::Subscriber image_sub_;
  image_transport::Publisher image_pub_;

public:
  ImageConverter() :
      it_(nh_)
  {
    // subscribe to input video feed and publish output video feed
    if (caseSwitch == 0)
      image_sub_ = it_.subscribe(CAMERA2_TOPIC, 1, &ImageConverter::imageCb, this);
    else
      image_sub_ = it_.subscribe(CAMERA1_TOPIC, 1, &ImageConverter::imageCb, this);

    image_pub_ = it_.advertise(OUTPUT_IMAGE, 1);
    marker_pub = nh_.advertise<visualization_msgs::Marker>("visualization_marker", 1);
    cv::namedWindow(OPENCV_WINDOW);

    // fill objectPoints vector
    int booard_h = chessboardSize.height;
    int booard_w = chessboardSize.width;
    for (int y = 0; y < booard_h; ++y)
    {
      for (int x = 0; x < booard_w; ++x)
        objectPoints.push_back(cv::Point3f(x * squareSize, y * squareSize, 0));
    }

  }

  ~ImageConverter()
  {
    cv::destroyWindow(OPENCV_WINDOW);
  }

  // import intrinsics for the cameras
  void importCalibData()
  {

    try
    {
      if (caseSwitch == 0)
      {
        cv::FileStorage fs(ARDRONE_EXTERNAL_CAMERA, cv::FileStorage::READ);
        fs["camera_matrix"] >> intrinsics;
        fs["distortion_coefficients"] >> distortion;
        MARKER_BROADCASTER = "/ardrone_base_externalcam";
      }
      else if (caseSwitch == 1)
      {
        cv::FileStorage fs(ARDRONE_FRONT_CAMERA, cv::FileStorage::READ);
        fs["camera_matrix"] >> intrinsics;
        fs["distortion_coefficients"] >> distortion;
        MARKER_BROADCASTER = "/ardrone_base_frontcam";
      }
      else if (caseSwitch == 2)
      {
        cv::FileStorage fs(ARDRONE_BOTTOM_CAMERA, cv::FileStorage::READ);
        fs["camera_matrix"] >> intrinsics;
        fs["distortion_coefficients"] >> distortion;
        MARKER_BROADCASTER = "/ardrone_base_bottomcam";
      }

    }
    catch (exception &e)
    {
      cout << " error reading camara_info.yaml" << e.what() << endl;
    }

  }

  // marker broadcaster
  void markerBroadcaster(cv::Mat rmat, cv::Mat tvec)
  {

    visualization_msgs::Marker points;
    points.header.frame_id = MARKER_BROADCASTER;
    points.header.stamp = ros::Time::now();
    points.ns = "chessboard_marker";
    points.action = visualization_msgs::Marker::ADD;
    points.type = visualization_msgs::Marker::POINTS;
    points.pose.orientation.w = 1.0;
    points.id = 0;
    points.scale.x = 0.01;
    points.scale.y = 0.01;
    points.color.r = 1.0;
    points.color.g = 0.0;
    points.color.b = 1.0;
    points.color.a = 1.0;

    /*------ for the marker points   -------*/
    geometry_msgs::Point point_cur;
    vector<cv::Mat> objPointsInCamFrame;
    cv::Mat cam_point, c_point;
    cv::Point3f pointInChessFrame;

    // convert points from world view to camera view, save them as a Mat vector filled with (x,y,z) points
    for (int i = 0; i < int(objectPoints.size()); i++)
    {
      pointInChessFrame = objectPoints.at(i); //a point in world (chessboard) coordinate frame
      cv::Mat m = (cv::Mat_<double>(3, 1) << pointInChessFrame.x, pointInChessFrame.y, pointInChessFrame.z); // convert a Point3f to Mat

      cam_point = rmat * m + tvec; // transform:point (X,Y,Z) from world coordinate frame to (x,y,z) camera coordinate frame
      cv::Point3f p(cam_point); // convert: from Mat point to Point3f
      point_cur.x = p.x; // convert: from Point3f point to geometry_msgs point
      point_cur.y = p.y;
      point_cur.z = p.z;

      /* uncomment to see the points
       cout << "p.x: " << p.x << endl;
       cout << "p.y: " << p.y << endl;
       cout << "p.z: " << p.z << endl;
       */
      points.points.push_back(point_cur);
    }

    marker_pub.publish(points);
  }

  // Callback Function
  void imageCb(const sensor_msgs::ImageConstPtr& msg)
  {
    cv_bridge::CvImagePtr cv_ptr;
    try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }
    cv::cvtColor(cv_ptr->image, cv_ptr->image, cv::COLOR_BGR2GRAY);

    // check for chessboard corners
    vector<cv::Point2f> corner_current;
    bool found_current, solve_current;
    importCalibData();
    cv::Mat rvec_current, tvec_current, rmat_current, rvec_current2, tvec_current2;

    if (caseSwitchBoard == 0)
      found_current = findChessboardCorners(cv_ptr->image, chessboardSize, corner_current,
                                            cv::CALIB_CB_ADAPTIVE_THRESH + cv::CALIB_CB_NORMALIZE_IMAGE);
    else if (caseSwitchBoard == 1)
      found_current = findCirclesGrid(cv_ptr->image, chessboardSize, corner_current, cv::CALIB_CB_SYMMETRIC_GRID);

    if (found_current)
    {
      cv::cornerSubPix(cv_ptr->image, corner_current, chessboardSize, cv::Size(-1, -1),
                       cv::TermCriteria(cv::TermCriteria::EPS + cv::TermCriteria::COUNT, 50, 0.1));

      cv::cvtColor(cv_ptr->image, cv_ptr->image, cv::COLOR_GRAY2RGB);
      cv::drawChessboardCorners(cv_ptr->image, chessboardSize, corner_current, found_current);

      solve_current = cv::solvePnP(objectPoints, corner_current, intrinsics, distortion, rvec_current, tvec_current,
                                   false, CV_EPNP);

      cv::solvePnPRansac(objectPoints, corner_current, intrinsics, distortion, rvec_current2, tvec_current2, false, 100,
                         0.50, 100, cv::noArray(), CV_ITERATIVE);

      cv::Rodrigues(rvec_current, rmat_current);
      markerBroadcaster(rmat_current, tvec_current);
      /*
      cout << "rvec: " << rvec_current << endl;
      cout << "tvec: " << tvec_current << endl;
      cout << "rvec2: " << rvec_current2 << endl;
      cout << "tvec2: " << tvec_current2 << endl;
       */
    }

    // Update GUI Window
    cv::imshow(OPENCV_WINDOW, cv_ptr->image);
    cv::waitKey(3);

    // Output modified video stream
    image_pub_.publish(cv_ptr->toImageMsg());
  }

};

int main(int argc, char** argv)
{
  ros::init(argc, argv, "chessboard_checker");
  ImageConverter ic;
  ros::spin();
  return 0;
}
