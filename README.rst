===================
external_camera_tf
===================

This ROS package for the indigo distribution contributes to the design and implementation of a
replaceable camera system for the Parrot AR-Drone 2.0. A low-cost 3D-printable camera mount system was designed that can be used, despite the Parrot AR.Drone 2.0, for other robots. The design is compact, light and modular, allowing exchanging or adding parts as desired. Based on experiments with additional weight, the drones payload is perceived as critical to carry the system but feasible.

Docs:
http://external-camera-tf.readthedocs.org/en/latest/